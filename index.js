const express = require('express')
const bodyParser= require('body-parser')
const Sequelize = require('sequelize')
//const MongoClient = require('mongodb').MongoClient
const db = require('./db')

const app = express()
app.set('view engine', 'ejs')

app.use(bodyParser.urlencoded({extended: true}))

const sequelize = new Sequelize('asdf', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  // SQLite only
 // storage: 'path/to/database.sqlite'
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

// db.connection.connect()
// console.log("Connected!");

const Quotes = sequelize.define('quotes', {
  name: {
    type: Sequelize.STRING
  },
  quote: {
    type: Sequelize.STRING
  },
},{timestamps: false,}
);

// const updateQuote = (req, res) => {
//   quotes.update({/* params to update */}, function () {
//     if (err) throw err;
//     return res.json(user)
//   })
// }

// Quotes.sync({force: false}).then(() => {
//   // Table created
//   return Quotes.create({
//     name: 'John',
//     quote: 'Hancock'
//   });
// });
  app.get('/:name/:quote', function (req, res, next) {
    // Quotes.findOne({ where: {name: 'aProject'} }).then(project => {
    
    // })
    console.log(req.params.quote)
    console.log(req.params.name)

    Quotes.update(
      {quote: req.params.quote},
      {where: {name: req.params.name}}
    )
    // .then(function(rowsUpdated) {
    //   console.log(res.json(rowsUpdated))
    // })
    .catch(next)

    Quotes.findOne({ where: {name: req.params.name} }).then(quotes => {
      console.log(`quote ${quotes.quote}, of  ${quotes.name} has been updated.`);
    })
   })

   app.put('/:name', function (req, res, next) {
    
    Quotes.findOne({ where: {name: req.params.name} }).then(quotes => {
      console.log(`quote ${quotes.quote}, of  ${quotes.name} is about to be changed.`);
    })

    console.log(req.query.quote)
    console.log(req.params.name)

    Quotes.update(
      
      {quote: req.query.quote},
      {where: {name: req.params.name}}
    )
    .then(result => {console.log(result)})
    .catch(next)

  })



  app.get('/', (req, res) => {
      // var sql = "SELECT * FROM quotes";
      // db.connection.query(sql, function (err, rows) {
      //   if (err) throw err;
      //   res.render('index.ejs', {quotes : rows});
      // });

      Quotes.findAll().then(quotes => {
        res.render('index.ejs', {quotes : quotes});
        //console.log(quotes)
      })

  });

  //-----------------------------------------------------------------------------------------------------------------------------

  app.post('/quotes', (req, res) => {
    console.log(req.body.name);
    console.log(req.body.quote);
 

    // sequelize.query('INSERT INTO quotes (name, quote) VALUES (:nm,:qt)'
    // ,{ replacements: { nm: req.body.name ,qt: req.body.quote  }
    // ,type: sequelize.QueryTypes.INSERT })
    // .then(()=>{console.log(req.body)
    //       res.redirect('/')});



    Quotes.sync({force: false}).then(() => {
      // Table created
      return Quotes.create({
        name: req.body.name,
        quote: req.body.quote
      });
    }).then(()=>{console.log(req.body)
       res.redirect('/')});

      
    });
  




  //----------------------------------------------------------------------------------------------------------------------------
// var db

//   MongoClient.connect('mongodb://admin:aa1111@ds235732.mlab.com:35732/asdf', (err, client) => {
//     if (err) return console.log(err)
//     db = client.db('asdf') // whatever your database name is
//     app.listen(3000, () => {
//       console.log('listening on 3000')
//     })
//   })

app.listen(3000, () => console.log('Example app listening on port 3000!'))